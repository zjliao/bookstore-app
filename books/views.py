from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic.edit import (
    CreateView, DeleteView, UpdateView,
)
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

from .forms import BookForm
from .models import Book

class BookDetailView(DetailView):
    """A view to display a book
    """
    model = Book
    template_name = 'books/book_view.html'

class BookListView(ListView):
    """A view to display the book list
    """
    model = Book
    template_name = 'books/index.html'

class BookCreateView(LoginRequiredMixin, CreateView):
    """A view to add a book to the book list
    """
    model = Book
    template_name = 'books/book_create.html'
    fields = '__all__'
    success_url = reverse_lazy('book-list')

class BookDeleteView(LoginRequiredMixin, DeleteView):
    """A view to remove a book from the book list
    """
    model = Book
    template_name = 'books/book_delete.html'
    success_url = reverse_lazy('book-list')

class BookUpdateView(LoginRequiredMixin, UpdateView):
    """A view to modify a book in the book list
    """
    model = Book
    form_class = BookForm
    template_name = 'books/book_update.html'
    success_url = reverse_lazy('book-list')
