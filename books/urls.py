from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^create/$', views.BookCreateView.as_view(), name='book-create'),
    url(r'^(?P<pk>\d+)/$', views.BookDetailView.as_view(), name='book-view'),
    url(r'^(?P<pk>\d+)/delete/$', views.BookDeleteView.as_view(), name='book-delete'),
    url(r'^(?P<pk>\d+)/update/$', views.BookUpdateView.as_view(), name='book-update'),
]
