from django.core.validators import RegexValidator
from django.db import models

class Book(models.Model):
    isbn = models.CharField(
        max_length=13,
        primary_key=True,
        validators=[
            RegexValidator(
                regex=r'^\d+\Z',
                message='Enter a valid ISBN.',
            ),
        ],
    )
    title = models.CharField(max_length=200)
    author = models.CharField(max_length=100)
    genre = models.CharField(max_length=100)
    price = models.DecimalField(max_digits=6, decimal_places=2)

    def __str__(self):
        return '{} by {}'.format(self.title, self.author)
